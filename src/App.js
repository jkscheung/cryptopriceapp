import React, { Component } from 'react';
import { Card, CardBody, CardColumns, CardSubtitle, CardTitle, Container, Col, Jumbotron, Row } from 'reactstrap';
import moment from 'moment'
import socketIOClient from 'socket.io-client';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      // To store currency data from server
      data: {},
      // Endpoint for loadbalancer
      endpoint: "http://localhost:4000"
    };
  }
  componentDidMount() {
    // Socket.io client init
    const { endpoint } = this.state;
    const socket = socketIOClient(endpoint);
    // Update local state when new data arrives
    socket.on("update", newData => {
      const pair = `${newData.ticker.base.toLowerCase()}-${newData.ticker.target.toLowerCase()}`;
      this.setState((prevState) => {
        return {data: Object.assign(prevState.data, { [pair]: newData })};
      });
    });
  }
  render() {
    // Card formatting for each currency. Aggregate all cards.
    const { data } = this.state;
    const cards = Object.keys(data).map((pair) => {
      return (
        <Card key={[pair]}>
          <CardBody>
            <CardTitle tag="h3">{data[pair].ticker.base}</CardTitle>
            <CardSubtitle className="text-warning">
              {data[pair].ticker.price === "" ? "Loading..." : `${data[pair].ticker.target} ${data[pair].ticker.price}`}
            </CardSubtitle>
            <div className="text-muted">
              <Row>
                <Col>volume:</Col>
                <Col>change:</Col>
              </Row>
              <Row>
                <Col>{data[pair].ticker.volume === "" ? "-" : data[pair].ticker.volume}</Col>
                <Col className={data[pair].ticker.change > 0 ? "text-success" : "text-danger"}>
                  {data[pair].ticker.change === "" ? "-" : data[pair].ticker.change}
                </Col>
              </Row>
              <Row>
                <Col style={{ textAlign: "right" }}>
                  <small>
                    {data[pair].timestamp === "" ? "" : moment.unix(data[pair].timestamp).fromNow()}
                  </small>
                </Col>
              </Row>
            </div>
          </CardBody>
        </Card>
      )
    });
    // Main layout
    return (
      <div>
        <Jumbotron fluid style={{ textAlign: "center" }}>
          <Container fluid>
            <h1 className="display-4">Cryptocurrency Realtime Price</h1>
          </Container>
        </Jumbotron>
        <Container>
          <CardColumns style={{ textAlign: "left" }}>
            {cards}
          </CardColumns>
        </Container>
      </div>
    );
  }
}

export default App;

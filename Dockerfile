FROM node:9.8.0-alpine

RUN mkdir -p /home/web/build
COPY build /home/web/build
WORKDIR "/home/web"

RUN yarn global add serve
EXPOSE 8080

ENTRYPOINT ["serve", "-s", "build", "-p", "8080"]
